import 'reflect-metadata';
import express, { NextFunction, Request, Response } from 'express';
import cors from 'cors';
import config from './config';
import APIError from '../src/helpers/APIError';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import router from '../src/index.route';
import { ValidationError as JoiValidationError} from 'joi';
import { ValidationError } from 'express-validation';
import httpStatus from 'http-status';
import errorcode from '../src/constants/errorCode';

const app = express();
const corsOptions = {
	origin(
		origin: string | undefined,
		callback: (arg0: APIError | null, arg1: boolean) => void
	){
		// eslint-disable-next-line no-console
		if (origin === undefined) {
			// Allow request from other server
			callback(null, true);
		} else if (config.corsWhiteList.indexOf(origin) !== -1) {
			callback(null, true);
		} else {
			callback(new APIError('Not allowed by CORS'), false);
		}  
	},
	optionsSuccessStatus: 200,
};

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(helmet());

//mount all routes on /api path
app.use('/api', router);

// Error handling
app.use((err: Error | ValidationError, req: Request, res: Response, next: NextFunction) => {
	if (err instanceof ValidationError) {
		const detailsBody: JoiValidationError[] | undefined = (err as ValidationError).details.body;
		const detailsQuery: JoiValidationError[] | undefined = (err as ValidationError).details.query;
		if (detailsBody) {
			const errorMessageArr: string[] = detailsBody.map(
				(thizBody) => thizBody.message
			);
			const error = new APIError(
				errorMessageArr.join(', '),
				httpStatus.BAD_REQUEST,
				errorcode.VALIDATE
			);
			return next(error);
		}
		if (detailsQuery) {
			const errorMessageArr: string[] = detailsQuery.map(
				(thizBody) => thizBody.message
			);
			const error = new APIError(
				errorMessageArr.join(', '),
				httpStatus.BAD_REQUEST,
				errorcode.VALIDATE
			);
			return next(error);
		}
		const error = new APIError(
			err.message,
			httpStatus.BAD_REQUEST,
			errorcode.VALIDATE
		);
		return next(error);
	}

	// if error is not an instanceOf APIError, convert it.
	if (!(err instanceof APIError)) {
		const apiError = new APIError(
			err.message,
			httpStatus.INTERNAL_SERVER_ERROR,
			errorcode.EXCEPTION,
			'',
			err.stack
		);
		return next(apiError);
	}
	return next(err);
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
	const err = new APIError(
		'NOT FOUND',
		httpStatus.NOT_FOUND,
		errorcode.OBJECT_NOT_FOUND
	);
	return next(err);
});

export default app;