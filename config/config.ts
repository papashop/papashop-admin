import Joi from 'joi';
import dotenv from 'dotenv';

dotenv.config();

const EnvSchema = Joi.object({
	NODE_ENV: Joi.string().valid('development', 'production', 'local').default('development'),
	CORS_WL: Joi.string(),
	PORT: Joi.number().default(3001),
}).unknown()
	.required();

interface ConfigInterface {
  corsWhiteList: Array<string>;
//   env: 'development' | 'production' | 'local';
  port: number;
//   accessTokenSecret: string;
//   refreshTokenSecret: string;
//   serverURL: string;
}

const { error, value: envVars } = EnvSchema.validate(process.env);

if (error) {
	throw new Error(`Config validation error: ${error.message}`);
}

const config: ConfigInterface = {
	corsWhiteList: envVars.CORS_WL.split(','),
	port: envVars.PORT
};

export default config;
