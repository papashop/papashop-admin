FROM node:18.16.1

# working directory is directory where you run all command
# the directory where if we copy any files to container.
WORKDIR /app

# an optimization trick - copy package.json and run npm install before copy source code
# explain trick: https://youtu.be/gm_L69NHuHM?t=877
COPY package.json . 


# defind arg in docker-compose file
ARG NODE_ENV
RUN if [ "$NODE_ENV" = "development" ]; \
            then npm install; \
            else npm install --only=production; \
            fi
# RUN npm install

COPY . ./

# expose port
EXPOSE 5000

# compile typescript to /build
RUN npm run build


